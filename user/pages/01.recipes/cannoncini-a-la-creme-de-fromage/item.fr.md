---
title: 'Cannoncini à la crème de fromage'
media_order: featured.jpeg
published: true
publish_date: '01/08/2021 02:00 am'
taxonomy:
    category:
        - cannoncini
cache_enable: false
visible: true
ingredients_title: "Ingredients :"
ingredients:
    -
        title: null
        list:
            - '500 g. de pâte feuilletée'
            - '1 Jaune d''oeuf'
            - Ricotta
            - '3 cuillères à soupe de Parmigiano Reggiano'
    -
        title: null
        list:
            - Beurre
            - Sel
            - '1 verre de Lait'
            - 'Poivre blanc'
---

### Préparation:

Coupez des bandes de pâte feuilletée d'environ 2 cm de largeur et 20 cm de longueur que vous humidifierez légèrement.  

Prenez des tubes en métal d'1 cm de diamètre et d'environ 8 cm de long.  

Badigeonnez-les de beurre et enroulez autour de chacun d'entre eux les bandes de pâtes feuilletées de manière à former une spirale, en laissant les bords se chevaucher légèrement afin de ne pas laisser d'espace vide.  

Appuyez légèrement à mesure que vous enroulez la bande de pâte afin d'éviter que le cannoncino s'ouvre durant la cuisson.  

Une fois cette étape terminée, disposez les cannoncini sur une plaque recouverte de papier sulfurisé et badigeonnez-les de jaune d'œuf dilué dans une cuillère à soupe d'eau froide.

Saupoudrez-les d'un soupçon de sel et mettez-les au four préchauffé à 200 °C pendant une vingtaine de minutes.

Pour la farce, mettez la Ricotta Galbani dans un bol et travaillez-la un peu à l'aide de la cuillère en bois afin de la rendre mousseuse.  

Ajoutez le Parmigiano Reggiano et le poivre et, tout en continuant à mélanger, du lait, jusqu'à obtenir une crème légère et moelleuse.

Lorsque les cannoncini sont bien gonflés et dorés, sortez-les du four et laissez-les refroidir un peu avant de retirer délicatement les tubes.

Une fois froides, farcissez-les avec la crème au fromage à l'aide d'une poche à douille cannelée.