---
title: 'Penne fondants aux aubergines et aux jambons croustillant'
published: true
date: '01/01/2021 12:00 pm'
visible: true
taxonomy:
    category:
        - penne
---

### Préparation :
Couper les tranches de jambon cru en lamelles, et les faire cuire dans une poêle très chaude jusqu'à ce que le jambon soit devenu croustillant.  

Nettoyer et couper en petits dés l'aubergine.  

Dans une poêle très chaude, faire cuire les aubergines en y mettant un peu de sel, sans ajouter de matières grasses au départ pour éviter que les aubergines ne deviennent molles.  

Ajouter un filet d'huile d'olive extra vierge, et terminer la cuisson.  

Pendant ce temps-là, cuire les pâtes type pennes très al dente, et terminer la cuisson dans une poêle, en ajoutant une petite louche d'eau de cuisson.  

Ajouter quelques feuilles de basilic et les laisser devenir fondantes, avec la Mozzarella, et une poignée de Gran Gusto.  

Servir à l'assiette et décorer chaque assiette avec le jambon croustillant.

### Ingrédients : (Pour 4 personnes)
- 400 g. de pâtes
- 2 aubergines
- 1 Mozzarella 400g
- 100 g. de Jambon cru
- Huile d'olive extra vierge
- Gran Gusto
- Basilic
- Sel