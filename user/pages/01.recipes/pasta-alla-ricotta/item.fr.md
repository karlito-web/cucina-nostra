---
title: 'Pasta alla Ricotta'
cache_enable: false
media_order: featured.jpeg
publish_date: '01/08/2021 02:00 am'
published: true
visible: true
taxonomy:
    category:
        - pasta
        - ricotta
ingredients_title: Ingrédients
ingredients:
  - title: null
    list:
      - Lorem ipsum, 200g
      - Dolor sit amet 20dl
      - 80g sugar
      - 1 yolk
      - Salt
      - Water 0.5l
      - Milk 1l
  - title: null
    list:
      - Lorem ipsum, 200g
      - Dolor sit amet 20dl
      - 80g sugar
      - 1 yolk
      - Salt
      - Water 0.5l
      - Milk 1l
---

# Pasta alla Ricotta

### Ingrédients : (Pour 4 personnes)
- 320 g. de Pâtes
- 250 g. de Ricotta Galbani
- 3 cuillère(s) à café de Cannelle

### Préparation :
Mettre les pâtes à cuire. Entretemps mettre la ricotta au fond d’une soupière.
Quand la cuisson est presque finie, verser sur la ricotta environ 3 cuillères d’eau de cuisson des pâtes et écraser avec la fourchette.
Égoutter les pâtes, verser dans la soupière et server.
Rajouter si vous le souhaitez les 3 cuillères à café de cannelle.