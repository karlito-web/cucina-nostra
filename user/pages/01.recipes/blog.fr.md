---
title: Recettes
media_order: header.jpg
taxonomy:
    category:
        - food
content:
    items:
        - '@self.children'
    limit: 15
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

## my blog description
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.