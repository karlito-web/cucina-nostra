---
title: 'Fusilli au pesto rouge et à la ricotta'
cache_enable: false
media_order: featured.jpeg
publish_date: '01/08/2021 02:00 am'
published: true
visible: true
taxonomy:
    category:
        - fusilli
        - ricotta
ingredients_title: Ingrédients (Pour 4 personnes)
ingredients:
  - title: null
    list:
        - 320 g. de pâtes
        - 200 g. de ricotta
        - 150 g. de tomates séchées
        - 40 g. de Grana Padano
  - title: null
    list:
        - Amandes
        - Basilic
        - Gousse d'ail
        - Huile d'olive extra-vierge
        - Sel
---

### Préparation :

Préparer les pâtes type fusilli au pesto de tomates séchées à l'huile et à la ricotta, c'est vraiment simple.

Dans un mixeur, hacher les tomates séchées égouttées, les amandes, le pecorino, le basilic, l'ail, une pincée de sel et la quantité d'huile nécessaire pour obtenir un pesto homogène et régulier. 

Y incorporer la Ricotta et mélanger avec soin. 

Cuire les pâtes al dente, égoutter les fusilli, les assaisonner avec la crème obtenue, et servir immédiatement. 

Saupoudrez le tout de Grana Padano ou Parmigiano Reggiano.