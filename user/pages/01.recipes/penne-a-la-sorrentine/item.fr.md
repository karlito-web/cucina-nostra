---
title: 'Penne à la sorrentine'
published: true
taxonomy:
    category:
        - penne
visible: true
---


### Ingrédients : (Pour 4 personnes)
- 350 g. de penne lisce
- 600 g. de Tomates
- 300 g. de Mozzarella
- 2 cuillère(s) à soupe de Parmigiano Reggiano
- 2 cuillère(s) à soupe d' huile d'olive vierge extra
- 2 Gousses d'ail
- Origan
- Basilic
- Sel
- Poivre

### Préparation :
Plongez les tomates S.Marzano bien mûres dans de l'eau en ébullition pendant quelques secondes, puis passez-les sous l'eau froide et pelez-les, épépinez-les et hachez-les grossièrement. Faites chauffer l'huile dans une poêle et faites blondir les gousses d'ail hachées. Ajoutez les tomates, salez et poivrez puis parfumez le tout avec une pincée d'origan et quelques feuilles de basilic. Faites cuire à feu vif pendant environ un quart d'heure et pendant ce temps, faites cuire les pâtes. Coupez la Mozzarella en fines tranches. Egouttez les pâtes (qui doivent être al dente) et versez-les dans un plat à gratin légèrement huilé. Recouvrez-les de tranches de mozzarella, puis de sauce tomate et saupoudrez de Parmigiano Reggiano. Enfournez à 200 °C pendant environ 10 minutes, jusqu'à ce que la mozzarella ait complètement fondu et se soit amalgamée à la sauce tomate afin de former une sauce épaisse et parfumée. Servez chaud.
