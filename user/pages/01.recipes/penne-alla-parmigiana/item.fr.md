---
title: 'Penne alla parmigiana'
visible: true
---

# Penne alla parmigiana

### Ingrédients : (Pour 6 personnes)
- 200 g. de Ricotta
- 30 g. de Parmigiano Reggiano D.O.P
- 100 g. de Gorgonzola
- 300 g. de Penne
- 6 tranche(s) de Jambon de Parme
- 6 feuille(s) de Basilic
- 200 g. de Tomates
- 1 gousse(s) d' Ail
- 100 g. de Courgettes
- 30 g. d' Oignons Hachés
- 5 cl. d' Huile d'olive
- Sel
- Poivre

### Préparation :
Faites cuire les pâtes.
Faites chauffer dans une poêle de l’huile d’olive, mettre les oignons et l’ail haché et faire cuire 2 mn.
Couper les courgettes en petits cubes et les ajouter dans la poêle, cuire 5 mn.
Mixer le basilic avec un peu d’eau et l'ajouter aux courgettes, ajouter ensuite la ricotta et faire cuire encore 5 mn.
Couper le jambon en fines lanières et les tomates en petits cubes.
Mettre les pâtes cuites dans la sauce courgette basilic ricotta, mettre ensuite le jambon de Parme et les tomates dedans.
Servir avec des cubes de gorgonzola et du Parmigiano Reggiano D.O.P.
Décorer avec des feuilles de basilic
