---
title: 'Gnocchetti à la crème de petits pois frais et à la mozzarella'
cache_enable: false
media_order: featured.jpeg
publish_date: '01/08/2021 02:00 am'
published: true
visible: true
taxonomy:
    category:
        - gnocchetti
        - mozzarella
ingredients_title: Ingrédients
ingredients:
  - title: null
    list:
      - Lorem ipsum, 200g
      - Dolor sit amet 20dl
      - 80g sugar
      - 1 yolk
      - Salt
      - Water 0.5l
      - Milk 1l
  - title: null
    list:
      - Lorem ipsum, 200g
      - Dolor sit amet 20dl
      - 80g sugar
      - 1 yolk
      - Salt
      - Water 0.5l
      - Milk 1l
---

# Gnocchettis à la crème de petits pois frais et à la mozzarelle

### Ingrédients : (Pour 4 personnes)
- 400 g. de gnocchettis sardes
- 2 paquet(s) de Mozzarelle Galbani
- 500 g. de petits pois frais
- 1 Oignon rouge
- 2 bouillons de légumes
- 20 g. de Beurre
- 20 g. de Grana Padano D.O.P Galbani
- 20 g. de Parmigiano Reggiano D.O.P Galbani
- Coriandre
- Poivre blanc
- Gnocchi à la romaine
- Préparation
- Coupez les gnocchi à la romaine en rondelles et disposez-les dans un plat à gratin, en laissant les bords se chevaucher légèrement. Recouvrez de Mozzarella Cucina Galbani et ajoutez quelques copeaux de beurre. Parsemez de Parmigiano Reggiano D.O.P Galbani et faites cuire à 200 °C pendant 15 minutes.
- Ingrédients
- (Pour 4 personnes)
- 200 g. de Mozzarella Cucina Galbani
- 1 Rouleau de gnocchis
- Parmigiano Reggiano D.O.P Galbani
- Beurre

### Préparation :
Peler et couper finement l'oignon rouge, et le faire rissoler dans une petite poêle avec de l'huile d'olive extra vierge. ajouter les petits pois et mettre à cuire pendant 20 minutes, en ajoutant un petit peu de bouillon de légumes chaud à la fois. Saler. Quand ils sont bien tendres, les enlever du feu et les passer au mixeur jusqu'à obtenir une pâte épaisse et crémeuse. A ce moment-là, passer la pâte obtenue dans un chinois pour éliminer d'éventuels morceaux de l'enveloppe des petits pois. Mettre ensuite la crème dans un grand bol, et y ajouter le poivre blanc, les grains de coriandre, le Grana Padano D.O.P Galbani et le Parmigiano Reggiano D.O.P Galbani. Bien mélanger et réserver. Pendant ce temps-là, faire cuire les pâtes dans de l'eau bouillante salée, et couper la mozzarelle boule Galbani en petits dés. Quand les pâtes sont cuites, les passer à la poêle avec la crème et la mozzarelle boule Galbani jusqu'à ce qu'elles deviennent fondantes. Servir chaud.