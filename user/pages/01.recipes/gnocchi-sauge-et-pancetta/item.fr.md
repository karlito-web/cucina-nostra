---
title: 'Gnocchi sauge et pancetta'
cache_enable: false
media_order: featured.jpeg
publish_date: '01/08/2021 02:00 am'
published: true
visible: true
taxonomy:
    category:
        - gnocchi
        - pancetta
ingredients_title: Ingrédients (Pour 4 personnes)
ingredients:
  - title: null
    list:
        - 400 g. de gnocchis
        - 80 g. de Pancetta
        - 100 g. de Mozzarella
        - 10 feuille(s) de Sauge
  - title: null
    list:
        - 100 g. de Beurre
        - 100 g. de Parmigiano Reggiano D.O.P
        - Sel
        - Poivre
---

### Préparation :
Si vous cherchez des idées de plats faciles et rapides à cuisiner, voici une délicieuse recette originale qui se prépare en seulement 10 minutes : elle vous fera gagner beaucoup de temps. C’est une recette facile comme un jeu d’enfant. Cette préparation culinaire plaira éventuellement à ceux qui apprécient des plats simples mais savoureux comme la purée de pomme de terre ou la tarte aux épinards et au fromage.

Pour commencer, faites griller la pancetta dans une poêle sans ajouter de matière grasse, jusqu'à ce qu'elle devienne croustillante.

Pendant ce temps, faites fondre le beurre avec la sauge puis, une fois que le beurre a bien fondu, ajoutez la pancetta puis les gnocchi que vous aurez fait cuire dans une grande casserole d'eau bouillante.

Même si cela reste facultatif, vous pouvez ajouter de la noix muscade à votre préparation.

Pour cette préparation, il faut veiller à ce que le plat ait une texture assez moelleuse qui fond dans la bouche.

Parsemez de dés de Mozzarella et servez chaud.

En plus des dés de Mozzarella, vous pouvez ajouter à votre recette une petite variété de garnitures  telles que des dés de poulet, de saumon ou de petites  tranches  de parmesan.

Veillez à servir ce plat encore frais et pour la sauce, vous pourrez ajouter soit un peu de farine ou de crème fraîche pour la rendre plus épaisse.

Dégustez votre plat de Gnocchi sauge et pancetta accompagné d’un risotto ou d’une salade de légumes.

N’hésitez à vous abonner à la newsletter, si vous avez des questions sur la recette ou que vous souhaitiez participer aux votes. Vous pouvez aussi recevoir d’autres recettes de spécialités italiennes faciles à préparer, grâce à nos newsletters.