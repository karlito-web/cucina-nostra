---
title: 'Gnocchi au gorgonzola'
cache_enable: false
media_order: featured.jpeg
publish_date: '01/08/2021 02:00 am'
published: true
visible: true
taxonomy:
    category:
        - gnocchi
        - gorgonzola
ingredients_title: Ingrédients (Pour 6 personnes)
ingredients:
  - title: null
    list:
        - 500 g. de gnocchis
        - 100 g. de Gorgonzola
        - 100 g. de Mascarpone
        - 1 verre de Lait
  - title: null
    list:
        - Beurre
        - Poivre noir
        - Huile d'olive extra-vierge
        - Roquette
---

### Préparation :
Pour réaliser les gnocchis de pomme de terre au gorgonzola, vous devrez d'abord vous occuper de l'assaisonnement.

Faites fondre la noix de beurre dans un poêlon antiadhésif.

Une fois le beurre fondu, ajoutez le Gorgonzola et le Mascarpone.

Faites cuire à feu doux de manière à ce que les deux fromages fondent ensemble, puis ajoutez le lait tout en continuant à mélanger.

Le tout doit être bien amalgamé.

Salez et poivrez la sauce ainsi obtenue.

Laissez le mélange s'épaissir sans cesser de mélanger.

Une fois cette étape terminée, occupez-vous de la cuisson des gnocchis.

Portez l'eau à ébullition, salez et ajoutez une goutte d'huile d'olive vierge extra.

Faite-y cuire les gnocchis.

Lorsqu'ils remontent à la surface, égouttez-les puis mélangez-les à la sauce préalablement allongée d'un peu d'eau de cuisson. Servez bien chaud.