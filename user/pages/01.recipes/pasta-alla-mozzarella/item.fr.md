---
title: 'Pasta alla mozzarella'
cache_enable: false
media_order: featured.jpeg
publish_date: '01/08/2021 02:00 am'
published: true
visible: true
taxonomy:
    category:
        - mozzarella
ingredients_title: Ingrédients
ingredients:
  - title: null
    list:
        - 320 g. de pâtes
        - 100 g. de Mozzarella 125g
  - title: null
    list:
        - 1 cuillère(s) à soupe d’huile
        - 1 Tomate
        - Sel
        - Pâte d'anchois
        - Câpres
---

# Préparation :
Faire cuire les pâtes. 

Pendant ce temps, mettre la boule de Mozzarella coupée en tranches dans une soupière avec de l'huile. 

Egoutter les pâtes et les verser dans la soupière, mélanger le tout avec la tomate, les câpres et de la pâte d'anchois si vous le souhaitez et servir.