---
title: 'Conchiglioni à la crème de jambon'
cache_enable: false
media_order: featured.jpeg
publish_date: '01/08/2021 02:00 am'
published: true
visible: true
taxonomy:
    category:
        - conchiglioni
ingredients_title: Ingrédients (Pour 4 personnes)
ingredients:
  - title: null
    list:
        - 400 g. de conchiglioni
        - 50 g. de jambon cuit
        - 50 g. de Beurre
  - title: null
    list:
        - 125 g. de ricotta
        - Sel
---

### Préparation :
Battez le beurre dans une terrine, incorporez la Ricotta et travaillez le tout jusqu'à obtenir un mélange mousseux. 

Coupez le jambon cuit gourmande en allumettes et ajoutez-le à la mousse. 

Faites cuire les conchiglioni dans de l'eau salée bouillante, puis après les avoir égouttés, remplissez-les avec la crème au jambon. 

Servez bien chaud.